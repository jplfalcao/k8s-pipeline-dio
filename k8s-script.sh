#!/bin/bash

kubectl apply -f secrets.yml --record
kubectl apply -f backfront-deployment-1.yml --record
kubectl apply -f db-deployment-1.yml --record
kubectl apply -f load-balancer.yml --record
